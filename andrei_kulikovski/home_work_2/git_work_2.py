#task_1_1
a = 2
b = 3
c = a + b
print("сумма", c)
d = a - b
print("разность", d)
e = a * b
print("произведение", e)


#task_1_2
x = 6
y = 5
a = (x - y) / (1 + x * y)
print("ответ", a)


#task_1_3
a = 6
v = a ** 3
print("Обьем куба", v)
s = 4 * a ** 2
print("Площадь боковой поверхности", s)


#task_1_4
from math import sqrt
a = 5
b = 5
c = (a + b) / 2
print("среднее арифметическое", c)
d = sqrt(a * b)
print("среднее геометрическое", d)


#task_1_5
from math import sqrt
a = 3
b = 4
c = sqrt(a ** 2 + b ** 2)
print("гипотенуза", c)
s = 1/2 * (a * b)
print("площадь", s)